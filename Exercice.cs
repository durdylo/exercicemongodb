using System;
using MongoDB.Driver;
using MongoDB.Bson;

namespace mongodb.exercice;
public class Exercice
{
    private string? connectionString;
    private MongoClient client;
    public Exercice(){
        connectionString = "";
        client = new MongoClient(connectionString);

        if (connectionString == null)
        {
            Console.WriteLine("You must set your 'MONGODB_URI' environment variable. To learn how to set it, see https://www.mongodb.com/docs/drivers/csharp/current/quick-start/#set-your-connection-string");
        }
    }
    public async Task<dynamic> exo0 (){
        var collection = client.GetDatabase("test").GetCollection<BsonDocument>("salles");
        var filter = Builders<BsonDocument>.Filter.Eq("nom", "AJMI Jazz Club");
        return await collection.Find(filter).FirstOrDefaultAsync();
    }

    public void exo1(){
        var collection = client.GetDatabase("cesi").GetCollection<BsonDocument>("salles");
        var filter = Builders<BsonDocument>.Filter.Eq("smac", true);
        var documents = collection.Find(filter).ToList();
        foreach (var document in documents)
        {
            Console.WriteLine(document);
        }
    }

     public void exo2(){
        var collection = client.GetDatabase("cesi").GetCollection<BsonDocument>("salles");
        var filter = Builders<BsonDocument>.Filter.Gt("capacite", 1000);
        var documents = collection.Find(filter).ToList();
        foreach (var document in documents)
        {
            Console.WriteLine(document);
        }
    }
}
